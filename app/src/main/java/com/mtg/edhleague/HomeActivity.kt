package com.mtg.edhleague

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.os.Bundle
import android.content.DialogInterface
import android.content.Intent
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        initButtons()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            R.id.action_settings -> {
                Toast.makeText(applicationContext, "Click Settings", Toast.LENGTH_LONG).show()
                true
            }
            R.id.action_exit -> {
                Toast.makeText(applicationContext, "Click Exit", Toast.LENGTH_LONG).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initButtons(){
        buttonPlay.setOnClickListener { playGame() }
        buttonLeaguePlay.setOnClickListener {  }
        buttonLeaderboard.setOnClickListener {  }
    }

    private fun playGame(){
        var numberOfPlayers: Int = 4
        var startingLifeTotal: Int = 40

        val builder = AlertDialog.Builder(this)
        val title = TextView(this)
        title.text = "Play Game"
        title.gravity = Gravity.CENTER_HORIZONTAL
        title.textSize = 24f
        title.setTextColor(resources.getColor(R.color.orange))
        builder.setCustomTitle(title)

        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        params.leftMargin = 10; params.rightMargin = 10

        // Set up the input
        val numberPlayersTitle = TextView(this)
        numberPlayersTitle.text = "Number of Players?"
        numberPlayersTitle.textSize = 20f
        numberPlayersTitle.setTextColor(resources.getColor(R.color.orange))
        numberPlayersTitle.layoutParams = params

        val startingLifeTitle = TextView(this)
        startingLifeTitle.text = "Starting Life Total?"
        startingLifeTitle.textSize = 20f
        startingLifeTitle.setTextColor(resources.getColor(R.color.orange))
        startingLifeTitle.layoutParams = params

        val numberPlayersSpinner = Spinner(this)

        val playerCountChoices = resources.getStringArray(R.array.player_count_choices)
        val playerCountAdapter = ArrayAdapter<String>(this, R.layout.spinner_item, playerCountChoices)
        numberPlayersSpinner.adapter = playerCountAdapter
        numberPlayersSpinner.setSelection(2)
        numberPlayersSpinner.layoutParams = params
        numberPlayersSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
                numberOfPlayers = playerCountChoices[position].toInt()
            }

        }

        val startingLifeSpinner = Spinner(this)
        val startingLifeChoices = resources.getStringArray(R.array.starting_life_choices)
        val startingLifeAdapter = ArrayAdapter<String>(this, R.layout.spinner_item, startingLifeChoices)
        startingLifeSpinner.adapter = startingLifeAdapter
        startingLifeSpinner.setSelection(2)
        startingLifeSpinner.layoutParams = params
        startingLifeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                startingLifeTotal = startingLifeChoices[p2].toInt()
            }

        }

        val screenOnCheckBox = CheckBox(this)
//        screenOnCheckBox.gravity = Gravity.END
        val screenOnTitle = TextView(this)
        screenOnTitle.text = "Keep Screen On"
        screenOnTitle.textSize = 20f
        screenOnTitle.setTextColor(resources.getColor(R.color.orange))
        screenOnTitle.layoutParams = params
        val screenOnLinearLayout = LinearLayout(this)
        screenOnLinearLayout.orientation = LinearLayout.HORIZONTAL
        screenOnLinearLayout.addView(screenOnCheckBox)
        screenOnLinearLayout.addView(screenOnTitle)

        val backgroundLinearLayout = LinearLayout(this)
        backgroundLinearLayout.orientation = LinearLayout.VERTICAL
        backgroundLinearLayout.layoutParams = params
        backgroundLinearLayout.setBackgroundColor(resources.getColor(R.color.darkGrey))
        backgroundLinearLayout.addView(numberPlayersTitle)
        backgroundLinearLayout.addView(numberPlayersSpinner)
        backgroundLinearLayout.addView(startingLifeTitle)
        backgroundLinearLayout.addView(startingLifeSpinner)
        backgroundLinearLayout.addView(screenOnLinearLayout)

        builder.setView(backgroundLinearLayout)

        // Set up the buttons
        builder.setPositiveButton("OK",
            DialogInterface.OnClickListener { dialog, which -> startGame(numberOfPlayers, startingLifeTotal, screenOnCheckBox.isChecked) })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })

        val dialog = builder.create()
        dialog.window.setBackgroundDrawable(resources.getDrawable(R.drawable.player_border))
        dialog.show()
    }

    private fun startGame(numberPlayers: Int, startingLife: Int, screenOn: Boolean){
        val intent = Intent(this, PlayActivity::class.java)
        var bundle = Bundle()
        bundle.putInt("numberPlayers", numberPlayers)
        bundle.putInt("playerLife", startingLife)
        bundle.putBoolean("screenOn", screenOn)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}
