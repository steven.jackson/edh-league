package com.mtg.edhleague

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_play.*
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Handler
import android.os.Looper
import android.view.WindowManager


class PlayActivity: AppCompatActivity() {
    val activePlayers = ArrayList<Player>()
    val activeFragments = ArrayList<PlayerFragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)
        hideSystemUI()

        val bundle = intent.extras

        val numberPlayers = if (bundle != null) bundle.getInt("numberPlayers") else 6
        val startingLife = if (bundle != null) bundle.getInt("playerLife") else 40
        val screenOn = if (bundle != null) bundle.getBoolean("screenOn") else false

        if (screenOn) window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        initPlayerFragments(numberPlayers, startingLife)
        initLayout(frameLayout1, frameLayout2, frameLayout3, frameLayout4, frameLayout5,
            frameLayout6, frameContainer1, frameContainer2, this, activePlayers)
    }

    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    override fun onPause() {
        super.onPause()
        hideSystemUI()
    }

    private fun hideSystemUI(){
        window.decorView.apply {
            systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    companion object {
        private lateinit var frameLayout1: FrameLayout
        private lateinit var frameLayout2: FrameLayout
        private lateinit var frameLayout3: FrameLayout
        private lateinit var frameLayout4: FrameLayout
        private lateinit var frameLayout5: FrameLayout
        private lateinit var frameLayout6: FrameLayout

        private lateinit var frameContainer1: ConstraintLayout
        private lateinit var frameContainer2: ConstraintLayout

        private lateinit var context: Context

        private lateinit var activePlayers: ArrayList<Player>

        fun hideParentContainer(containerNumber: Int) {
            when (containerNumber) {
                1 -> frameLayout1.visibility = View.GONE
                2 -> frameLayout2.visibility = View.GONE
                3 -> frameLayout3.visibility = View.GONE
                4 -> frameLayout4.visibility = View.GONE
                5 -> frameLayout5.visibility = View.GONE
                6 -> frameLayout6.visibility = View.GONE
            }
            removePlayer(containerNumber)
            var count = 0
            for (child in frameContainer1.children) {
                if (child.visibility == View.VISIBLE) count++
            }
            if (count == 0) frameContainer1.visibility = View.GONE
            count = 0
            for (child in frameContainer2.children) {
                if (child.visibility == View.VISIBLE) count++
            }
            if (count == 0) frameContainer2.visibility = View.GONE
        }

        private fun removePlayer(playerNumber: Int){
            var playerToRemove: Player? = null
            for (player in activePlayers){
                if (player.playerNumber == playerNumber) {
                    playerToRemove = player
                    break
                }
            }
            activePlayers.remove(playerToRemove)
        }

        fun initLayout(
            fl1: FrameLayout, fl2: FrameLayout, fl3: FrameLayout,
            fl4: FrameLayout, fl5: FrameLayout, fl6: FrameLayout,
            fc1: ConstraintLayout, fc2: ConstraintLayout, context: Context,
            activePlayers: ArrayList<Player>
        ) {
            this.frameLayout1 = fl1
            this.frameLayout2 = fl2
            this.frameLayout3 = fl3
            this.frameLayout4 = fl4
            this.frameLayout5 = fl5
            this.frameLayout6 = fl6

            this.frameContainer1 = fc1
            this.frameContainer2 = fc2

            this.context = context

            this.activePlayers = activePlayers
        }
    }

    private fun initPlayerFragments(numberPlayers: Int = 6, startingLife: Int = 40){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val player1 = Player("Player 1", 1)
        activePlayers.add(player1)
        val playerFragment1 = PlayerFragment(1, startingLife, player1)
        activeFragments.add(playerFragment1)
        fragmentTransaction.add(R.id.frameLayout1, playerFragment1, "player_one")

        val player2 = Player("Player 2", 2)
        activePlayers.add(player2)
        val playerFragment2 = PlayerFragment(2, startingLife, player2)
        activeFragments.add(playerFragment2)
        fragmentTransaction.add(R.id.frameLayout2, playerFragment2, "player_two")

        if (numberPlayers >= 3){
            val player3 = Player("Player 3", 3)
            activePlayers.add(player3)
            val playerFragment3 = PlayerFragment(3, startingLife, player3)
            activeFragments.add(playerFragment3)
            fragmentTransaction.add(R.id.frameLayout3, playerFragment3, "player_three")
        } else frameLayout3.visibility = View.GONE

        if (numberPlayers >= 4) {
            val player4 = Player("Player 4", 4)
            activePlayers.add(player4)
            val playerFragment4 = PlayerFragment(4, startingLife, player4)
            activeFragments.add(playerFragment4)
            fragmentTransaction.add(R.id.frameLayout4, playerFragment4, "player_four")
        } else frameLayout4.visibility = View.GONE

        if (numberPlayers >= 5) {
            val player5 = Player("Player 5", 5)
            activePlayers.add(player5)
            val playerFragment5 = PlayerFragment(5, startingLife, player5)
            activeFragments.add(playerFragment5)
            fragmentTransaction.add(R.id.frameLayout5, playerFragment5, "player_five")
        } else frameLayout5.visibility = View.GONE

        if (numberPlayers >= 6){
            val player6 = Player("Player 6", 6)
            activePlayers.add(player6)
            val playerFragment6 = PlayerFragment(6, startingLife, player6)
            activeFragments.add(playerFragment6)
            fragmentTransaction.add(R.id.frameLayout6, playerFragment6, "player_six")
        } else frameLayout6.visibility = View.GONE

        fragmentTransaction.commit()

        for (playerFragment in activeFragments){
            playerFragment.cDamageInit(activePlayers)
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        hideSystemUI()
//        val runnable = Runnable { hideSystemUI() }

//        val handler = Handler(Looper.getMainLooper())
//        handler.postDelayed(runnable, 300)
    }
}