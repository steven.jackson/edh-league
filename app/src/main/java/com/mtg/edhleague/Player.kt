package com.mtg.edhleague

class Player {
    var name: String = ""
    var playerNumber: Int = 0

    constructor(name: String, playerNumber: Int){
        this.name = name
        this.playerNumber = playerNumber
    }
}