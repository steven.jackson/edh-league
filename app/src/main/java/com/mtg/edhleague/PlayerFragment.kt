package com.mtg.edhleague

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_player.*

class PlayerFragment: Fragment {

    var playerNumber: Int = 0
    var playerLife: Int = 0
    var commanderDamage: Int = 0
    var infectDamage: Int = 0
    var player: Player? = null

    val cDamages = ArrayList<cDamage>()

    constructor(playerNumber: Int, startingLife: Int, player: Player){
        this.playerNumber = playerNumber
        this.playerLife = startingLife
        this.player = player
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun cDamageInit(players: ArrayList<Player>){
        for (player in players){
            if (this.playerNumber != player.playerNumber) cDamages.add(cDamage((player)))
        }
    }

    private fun changePlayerName(){
        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        params.leftMargin = 10; params.rightMargin = 10

        val builder = AlertDialog.Builder(activity)
        val title = TextView(activity)
        title.text = "Change Player Name"
        title.gravity = Gravity.CENTER_HORIZONTAL
        title.textSize = 24f
        title.setTextColor(resources.getColor(R.color.orange))
        builder.setCustomTitle(title)

        val newName = EditText(activity)
        newName.setText(player?.name)
        newName.setTextColor(resources.getColor(R.color.lightGrey))
        newName.layoutParams = params

        val backgroundFrameLayout = FrameLayout(activity)
        backgroundFrameLayout.layoutParams = params
        backgroundFrameLayout.setBackgroundColor(resources.getColor(R.color.darkGrey))
        backgroundFrameLayout.addView(newName)

        val linearLayout = LinearLayout(activity)
        linearLayout.addView(backgroundFrameLayout)

        builder.setView(linearLayout)
        builder.setPositiveButton("OK",
            DialogInterface.OnClickListener{ dialog, which -> setName(newName) })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener{ dialog, which -> dialog.cancel() })
        val dialog = builder.create()
        dialog.window.setBackgroundDrawable(resources.getDrawable(R.drawable.player_border))
        dialog.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.show()
        dialog.window.decorView.systemUiVisibility = activity!!.window.decorView.systemUiVisibility
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }

    private fun setName(newName: EditText){
        textViewPlayer.text = newName.text
        this.player?.name = newName.text.toString()
    }

    private fun setDamage(){
        val activePlayers = (activity as PlayActivity).activePlayers

        val builder = AlertDialog.Builder(activity!!)
        val title = TextView(activity)
        title.text = "Damage"
        title.gravity = Gravity.CENTER_HORIZONTAL
        title.textSize = 24f
        title.setTextColor(resources.getColor(R.color.orange))
        builder.setCustomTitle(title)

        val linearLayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        linearLayoutParams.leftMargin = 10; linearLayoutParams.rightMargin = 10; linearLayoutParams.gravity = Gravity.CENTER_HORIZONTAL
        val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT)
        params.leftMargin = 10; params.rightMargin = 10; params.gravity = Gravity.CENTER_HORIZONTAL
//        params.weight = 1f
        val paramsButtons = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsButtons.leftMargin = 10; paramsButtons.rightMargin = 10; paramsButtons.gravity = Gravity.CENTER_HORIZONTAL
//        paramsButtons.weight = .02f

        val infectString = TextView(activity)
        infectString.text = resources.getString(R.string.infect)
        infectString.textSize = 20f
        infectString.gravity = Gravity.CENTER_HORIZONTAL
        infectString.setTextColor(resources.getColor(R.color.infectGreen))
        infectString.layoutParams = params

        var infectText = TextView(activity)
        infectText.text = infectDamage.toString()
        infectText.textSize = 20f
        infectText.width = 200
        infectText.gravity = Gravity.CENTER_HORIZONTAL
        infectText.setTextColor(resources.getColor(R.color.lightGrey))
        infectText.layoutParams = params

        val minusButton = Button(activity)
        minusButton.text = "-"
//        minusButton.setTextColor(resources.getColor(R.color.orange))
//        minusButton.setBackgroundResource(R.drawable.life_button)
        minusButton.layoutParams = paramsButtons
        minusButton.setOnClickListener{
            var num = infectText.text.toString().toInt()
            if (num > 0) {
                num--
                infectText.text = num.toString()
            }
        }

        val plusButton = Button(activity)
        plusButton.text = "+"
//        plusButton.setTextColor(resources.getColor(R.color.orange))
//        plusButton.setBackgroundResource(R.drawable.life_button)
        plusButton.layoutParams = paramsButtons
        plusButton.setOnClickListener{
            var num = infectText.text.toString().toInt() + 1
            infectText.text = num.toString()
        }

        val infectLayout = LinearLayout(activity)
        infectLayout.gravity = Gravity.CENTER_HORIZONTAL
        infectLayout.layoutParams = linearLayoutParams
        infectLayout.addView(minusButton); infectLayout.addView(infectText); infectLayout.addView(plusButton)

        val verticalLinearLayout = LinearLayout(activity)
        verticalLinearLayout.orientation = LinearLayout.VERTICAL
        verticalLinearLayout.layoutParams = linearLayoutParams
        verticalLinearLayout.setBackgroundColor(resources.getColor(R.color.darkGrey))
        verticalLinearLayout.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
        val divider = ImageView(activity)
        divider.layoutParams = params
        divider.setBackgroundColor(resources.getColor(R.color.lightGrey))
        verticalLinearLayout.dividerDrawable = resources.getDrawable(R.drawable.divider)
        verticalLinearLayout.addView(infectString); verticalLinearLayout.addView(infectLayout)

        val damages = ArrayList<TextView>()
        for (cDamage in cDamages){
            if (activePlayers.contains(cDamage.player)) {
                val name = TextView(activity)
                name.text = cDamage.player.name
                name.textSize = 20f
                name.gravity = Gravity.CENTER_HORIZONTAL
                name.setTextColor(resources.getColor(R.color.commanderRed))
                name.layoutParams = params

                val playerDamage = TextView(activity)
                playerDamage.text = cDamage.damage.toString()
                playerDamage.textSize = 20f
                playerDamage.width = 200
                playerDamage.gravity = Gravity.CENTER_HORIZONTAL
                playerDamage.setTextColor(resources.getColor(R.color.lightGrey))
                playerDamage.layoutParams = params
                damages.add(playerDamage)

                val decButton = Button(activity)
                decButton.text = "-"
//                decButton.setTextColor(resources.getColor(R.color.orange))
//                decButton.setBackgroundResource(R.drawable.life_button)
                decButton.layoutParams = paramsButtons
                decButton.height = 15
                decButton.setOnClickListener{
                    var number = playerDamage.text.toString().toInt()
                    if (number > 0){
                        number--
                        playerDamage.text = number.toString()
                    }
                }

                val incButton = Button(activity)
                incButton.text = "+"
//                incButton.setTextColor(resources.getColor(R.color.orange))
//                incButton.setBackgroundResource(R.drawable.life_button)
                incButton.layoutParams = paramsButtons
                incButton.setOnClickListener{
                    var number = playerDamage.text.toString().toInt() + 1
                    playerDamage.text = number.toString()
                }

                val horizontalLinearLayout = LinearLayout(activity)
                horizontalLinearLayout.gravity = Gravity.CENTER_HORIZONTAL
                horizontalLinearLayout.orientation = LinearLayout.HORIZONTAL
                horizontalLinearLayout.addView(decButton)
                horizontalLinearLayout.addView(playerDamage)
                horizontalLinearLayout.addView(incButton)

                verticalLinearLayout.addView(name)
                verticalLinearLayout.addView(horizontalLinearLayout)
            }
        }

        val scrollView = ScrollView(activity)

        scrollView.addView(verticalLinearLayout)
        builder.setView(scrollView)

        builder.setPositiveButton("OK",
            DialogInterface.OnClickListener{ dialog, which -> updateDamage(infectText.text.toString().toInt(), damages) })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener{ dialog, which -> dialog.cancel() })
        val dialog = builder.create()
        dialog.window.setBackgroundDrawable(resources.getDrawable(R.drawable.player_border))
        dialog.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        dialog.show()
        dialog.window.decorView.systemUiVisibility = activity!!.window.decorView.systemUiVisibility
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }

    private fun updateDamage(infectUpdate: Int, damages: ArrayList<TextView>){
        infectDamage = infectUpdate
        textViewInfectDamageNum.text = infectDamage.toString()

        var highestCommanderDamage: Int = 0
        for (i in 0 until damages.size){
            cDamages[i].damage = damages[i].text.toString().toInt()
            if (cDamages[i].damage > highestCommanderDamage) highestCommanderDamage = cDamages[i].damage
        }
        commanderDamage = highestCommanderDamage
        textViewCommanderDamageNum.text = highestCommanderDamage.toString()

        detectLoss()
    }

    private fun initButtons() {
        buttonPlusLife.setOnClickListener { incrementLife() }
        buttonPlusLife.setOnLongClickListener{
            plus10Life()
            true
        }
        buttonMinusLife.setOnClickListener { decrementLife() }
        buttonMinusLife.setOnLongClickListener{
            minus10Life()
            true
        }
        textViewPlayer.setOnClickListener { changePlayerName() }
        textViewCommanderDamageTitle.setOnClickListener{ setDamage() }
        textViewCommanderDamageNum.setOnClickListener{ setDamage() }
        textViewInfectDamageTitle.setOnClickListener{ setDamage() }
        textViewInfectDamageNum.setOnClickListener { setDamage() }
    }

    private fun incrementLife(){
        playerLife++
        textViewLife.text = playerLife.toString()
        detectLoss()
    }

    private fun plus10Life(){
        playerLife += 10
        textViewLife.text = playerLife.toString()
        detectLoss()
    }

    private fun decrementLife(){
        playerLife--
        textViewLife.text = playerLife.toString()
        detectLoss()
    }

    private fun minus10Life(){
        playerLife -= 10
        textViewLife.text = playerLife.toString()
        detectLoss()
    }

    private fun detectLoss(){
        if (playerLife <= 0 || infectDamage >= 11 || commanderDamage >= 21) {
            val builder = androidx.appcompat.app.AlertDialog.Builder(context!!)

            val playerName = player?.name
            val title = TextView(activity)
            title.text = "Has $playerName been defeated?"
            title.gravity = Gravity.CENTER_HORIZONTAL
            title.textSize = 24f
            title.setTextColor(resources.getColor(R.color.orange))
            builder.setCustomTitle(title)

            builder.setPositiveButton("Yes",
                DialogInterface.OnClickListener{ dialog, which -> PlayActivity.hideParentContainer(playerNumber) })
            builder.setNegativeButton("No",
                DialogInterface.OnClickListener{ dialog, which ->
                    playerLife = 1
                    textViewLife.text = playerLife.toString()
                    dialog.cancel() })
            val dialog = builder.create()
            dialog.window.setBackgroundDrawable(resources.getDrawable(R.drawable.player_border))
            dialog.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
            dialog.show()
            dialog.window.decorView.systemUiVisibility = activity!!.window.decorView.systemUiVisibility
            dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initButtons()
        initPlayer()
        textViewPlayer.text = player?.name
    }

    override fun onPause() {
        super.onPause()
    }

    private fun initPlayer(){
        textViewLife.text = playerLife.toString()
    }

    inner class cDamage(player: Player){
        var damage: Int = 0
        val player: Player = player
    }
}